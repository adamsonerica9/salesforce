{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedLists #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE LambdaCase #-}

module Salesforce.Bulk where

import Control.Lens
  ( (^.)
  , (^..)
  , (^?)
  )
import Control.Logging (errorL', warnS')
import Data.Aeson (Value, object, (.=))
import Data.Aeson.Key (fromText)
import Data.Aeson.Lens (AsValue (_String), key)
import Data.Aeson.Types (Pair)
import Data.CSV.Product
  ( FromLie
  , L (E, L, T, getPutOp)
  , P (P)
  , PutOpOf
  , R (R)
  , RC (RC)
  , RT (RT)
  , defRT
  , getT1

  , reT
  , recordColumns
  , tie
  , tieToProduct
  )
import Data.Csv ( FromField(..), ToField(..) )
import Data.JSOP (required)
import qualified Data.Map as M
import Generics.SOP
  ( I (I)
  , IsProductType
  , NP (Nil, (:*))
  )
import Generics.SOP.TH (deriveGeneric)
import Lib.Retry (rt)
import Pipes (Producer, each)
import qualified Pipes.Prelude as P
import Protolude
import Salesforce.API
  ( DataApi
  , IndexOf
  , IndexOf' (IndexOf, indexOf)
  , getAllRecords
  , gqueryX
  , indexP
  , mkIndex
  , sfIndex
  )
import Salesforce.Cassava (boolRT)
import Salesforce.Login (WithSF, withToken)
import Salesforce.Streaming.Query (queryStreaming)
import Servant
  ( Capture
  , Get
  , Header
  , Headers (getResponse)
  , JSON
  , NoContent
  , QueryParam
  , ReqBody
  , ResponseHeader (Header)
  , Tagged (..)
  , lookupResponseHeader
  , type (:>), DeleteNoContent, Post, PutCreated, Patch
  )
import Servant.CSV.Records
  ( CSV
  , CSVMulti (..)
  , CSVSingle (CSVIndexSingle)
  , DeleteL
  , Operation (Deleting, Putting, Querying, Updating)
  , PutL
  , QueryL
  , Single
  , UpdateL
  )
import Servant.Client (client)
import Streaming (Of, Stream)
import qualified Streaming.Prelude as S

type JobsModApi (x :: Symbol) b = DataApi ("jobs" :> x :> b)

type JobsIngestApi b = JobsModApi "ingest" b

ingestP :: Proxy "ingest"
ingestP = Proxy

queryPx :: Proxy "query"
queryPx = Proxy

type PostJobsApi x =
  JobsModApi x (ReqBody '[JSON] Value :> Post '[JSON] Value)

type PostIngestApi = PostJobsApi "ingest"

type RemoveJobsApi =
  JobsIngestApi (Capture "id" Text :> DeleteNoContent)

type GetJobStatusApi x =
  JobsModApi x (Capture "id" Text :> Get '[JSON] Value)

type GetJobs =
  JobsIngestApi (Get '[JSON] Value)

type PatchJobsApi =
  JobsIngestApi
    ( Capture "id" Text
        :> ReqBody '[JSON] Value
        :> Patch '[JSON] NoContent
    )

type ResultJobApi l =
  JobsModApi
    "query"
    ( Capture "id" Text
        :> "results"
        :> QueryParam "maxRecords" Int
        :> QueryParam "locator" Text
        :> Get
             '[CSV l 'Querying]
             (Headers '[Header "Sforce-Locator" Text] (Tagged l [QueryL l]))
    )

newtype SuccessfulResults (o :: Symbol) l = SuccessfulResults
  { si_Created :: Bool
  }
  deriving (Eq, Show)

deriveGeneric ''SuccessfulResults

instance CSVMulti l => CSVMulti (SuccessfulResults o l) where
  type CSVIndex (SuccessfulResults o l) = IndexOf o
  type CSVProducts (SuccessfulResults o l) = SuccessfulResults o l : CSVProducts l
  csvMulti _ = T (defRT "sf__Id") $
    L
      do
        RC $
          boolRT "sf__Created"
            :* Nil
      do getPutOp (reT (panic "not necessary") $ csvMulti (Proxy @l))

type SuccessfulResultJobApi o l =
  JobsIngestApi
    ( Capture "id" Text
        :> "successfulResults"
        -- :> Get '[CSV l 'Querying] ] (Tagged l [QueryL l]))
        :> Get
             '[CSV (SuccessfulResults o l) 'Querying]
             (Tagged (SuccessfulResults o l) [QueryL (SuccessfulResults o l)])
    )

newtype FailedResults (o :: Symbol) l = FailedResults
  { fr_Error :: Text
  }
  deriving (Eq, Show)

deriveGeneric ''FailedResults

instance CSVMulti l => CSVMulti (FailedResults o l) where
  type CSVIndex (FailedResults o l) = Maybe (IndexOf o)
  type CSVProducts (FailedResults o l) = FailedResults o l : CSVProducts l
  csvMulti _ = T (defRT "sf__Id") $
    L
      do
        RC $
          defRT "sf__Error"
            :* Nil
      do getPutOp (reT (panic "not necessary") $ csvMulti (Proxy @l))

type FailedResultJobApi o l =
  JobsIngestApi
    ( Capture "id" Text
        :> "failedResults"
        -- :> Get '[CSV l 'Querying] ] (Tagged l [QueryL l]))
        :> Get
             '[CSV (FailedResults o l) 'Querying]
             (Tagged (FailedResults o l) [QueryL (FailedResults o l)])
    )

type PutJobsApi l =
  JobsIngestApi
    ( Capture "id" Text
        :> ReqBody '[CSV l 'Putting] (Tagged l [PutL l])
        :> "batches"
        :> PutCreated '[JSON] NoContent
    )

type UpdateJobsApi l =
  JobsIngestApi
    ( Capture "id" Text
        :> ReqBody '[CSV l 'Updating] (Tagged l [UpdateL l])
        :> "batches"
        :> PutCreated '[JSON] NoContent
    )

type DeleteJobsApi l =
  JobsIngestApi
    ( Capture "id" Text
        :> ReqBody '[CSV l 'Deleting] (Tagged l [DeleteL l])
        :> "batches"
        :> PutCreated '[JSON] NoContent
    )

type JobO = "Job0"

postJ :: forall p (x :: Symbol). KnownSymbol x => p x -> [Pair] -> WithSF (IndexOf JobO)
postJ _ x = do
  v <- rt $ withToken $ \tk -> client (Proxy :: Proxy (PostJobsApi x)) tk $ object x
  case v ^? key "id" . _String of
    Nothing -> errorL' "post job failed"
    Just id -> pure $ mkIndex (Proxy @JobO) id

postIngestJ :: [Pair] -> WithSF (IndexOf JobO)
postIngestJ = postJ ingestP

removeJ :: IndexOf JobO -> WithSF ()
removeJ (IndexOf id) = void $ rt $ withToken $ \tk -> client (Proxy :: Proxy RemoveJobsApi) tk id

patchJ :: IndexOf JobO -> [Pair] -> WithSF ()
patchJ (IndexOf id) x = void $
  rt $
    withToken $
      \tk -> client (Proxy :: Proxy PatchJobsApi) tk id $ object x

putJ
  :: forall l.
  ( CSVMulti l
  )
  => IndexOf JobO
  -> Tagged l [PutL l]
  -> WithSF ()
putJ (IndexOf id) x = void $
  rt $
    withToken $
      \tk -> client (Proxy :: Proxy (PutJobsApi l)) tk id x

updateJ
  :: forall l.
  ( CSVMulti l
  )
  => IndexOf JobO
  -> Tagged l [UpdateL l]
  -> WithSF ()
updateJ (IndexOf id) x = void $
  rt $
    withToken $
      \tk -> client (Proxy :: Proxy (UpdateJobsApi l)) tk id x

deleteJ
  :: forall l.
  (CSVMulti l)
  => IndexOf JobO
  -> Tagged l [DeleteL l]
  -> WithSF ()
deleteJ (IndexOf id) x = void $
  rt $
    withToken $
      \tk -> client (Proxy @(DeleteJobsApi l)) tk id x

getSomeResults
  :: forall l.
  (CSVMulti l)
  => IndexOf JobO
  -> Int
  -> Maybe Text
  -> WithSF (Tagged l [QueryL l], Maybe Text)
getSomeResults (IndexOf id) n loc = rt $
  withToken $ \tk -> do
    r <- client (Proxy :: Proxy (ResultJobApi l)) tk id (Just n) loc
    let v = getResponse r
    jump <- case lookupResponseHeader r :: ResponseHeader "Sforce-Locator" Text of
      Header h -> pure h
      err -> throwIO . PatternMatchFail $ "Salesforce.Bulk.getSomeResults: Bad/missing response header Sforce-Locator: " <> show err
    pure $
      (v,) $ case jump of
        "null" -> Nothing
        rest -> Just rest

getJStatus :: forall x p. KnownSymbol x => p (x :: Symbol) -> IndexOf JobO -> WithSF Value
getJStatus _ (IndexOf id) = rt $ withToken $ \tk -> client (Proxy :: Proxy (GetJobStatusApi x)) tk id

getJobs :: WithSF [Value]
getJobs = rt $ getAllRecords $ withToken $ \tk -> client (Proxy :: Proxy GetJobs) tk

getResults
  :: forall l p o.
  (CSVMulti l)
  => IndexOf JobO
  -> p o
  -> WithSF (Tagged l [QueryL l])
getResults (IndexOf id) _ = rt $
  withToken $ \tk ->
    getResponse
      <$> client (Proxy :: Proxy (ResultJobApi l)) tk id Nothing Nothing

cleanJobs :: WithSF ()
cleanJobs = do
  jobs <- getJobs
  forM_
    do
      job <- jobs
      id <- job ^.. key "id" . _String . sfIndex
      jt <- job ^.. key "jobType" . _String
      status <- job ^.. key "state" . _String
      guard $ jt == "V2Ingest"
      pure (id, status)
    do
      \(job, status) -> do
        when (status == "UploadComplete") do
          patchJ job ["state" .= ("Aborted" :: Text)]
        when (status == "Open") do
          patchJ job ["state" .= ("Aborted" :: Text)]
        removeJ job

waitJob :: KnownSymbol x => Proxy (x :: Symbol) -> IndexOf JobO -> WithSF ()
waitJob p job = do
  v <- getJStatus p job
  case v ^. key "state" . _String of
    "JobComplete" -> pure ()
    "Failed" -> warnS' "bulk" "failed"
    _ -> liftIO (threadDelay 100_000) >> waitJob p job

--------------------------------------------------------------------------------------------
-- mid level interface
--------------------------------------------------------------------------------------------

bulkQuery
  :: forall l p a.
  ( KnownSymbol a
  , CSVMulti l
  )
  => Int
  -> p a
  -> Text
  -> WithSF (Tagged l [QueryL l])
bulkQuery records object' cond = fmap sequenceA $ P.toListM $ bulkQueryP records object' cond

bulkQueryP
  :: forall l p a.
  ( KnownSymbol a
  , CSVMulti l
  )
  => Int
  -> p a
  -> Text -- ^ where + oreder by part
  -> Producer (Tagged l (QueryL l)) WithSF ()
bulkQueryP records object' cond = do
  job <-
    lift $
      postJ
        queryPx
        [ "operation" .= ("query" :: Text)
        , "query" .= do
            do gqueryX (toS $ symbolVal object')
              (fmap fromText . recordColumns $ csvMulti (Proxy :: Proxy l))
              cond
        ]
  lift $ waitJob queryPx job
  let feed from' = do
        (result, cont) <- lift $ getSomeResults job records from'
        each $ sequence result
        traverse_ (feed . Just) cont
  feed Nothing

bulkQueryS
  :: forall a l.
  ( KnownSymbol a
  , CSVMulti l
  )
  => Int
  -> Text -- ^ where + oreder by part
  -> S.Stream (S.Of (Tagged a (Tagged l (QueryL l)))) WithSF ()
bulkQueryS records cond = do
  job <-
    lift $
      postJ
        queryPx
        [ "operation" .= ("query" :: Text)
        , "query" .= do
            do gqueryX (toS $ symbolVal (Proxy @a))
              (fmap fromText . recordColumns $ csvMulti (Proxy @l))
              cond
        ]
  lift $ waitJob queryPx job
  let feed from' = do
        (result, cont) <- lift $ getSomeResults job records from'
        S.each $ Tagged <$> sequence result
        traverse_ (feed . Just) cont
  feed Nothing

type FailedData l a =
  L
    (PutOpOf (CSVProducts l))
    I
    (Maybe (IndexOf a))
    (CSVProducts l)

data Failure l a = Failure
  {

  }
bulkInsert
  :: forall l z p a.
  ( KnownSymbol a
  , Ord z
  , CSVIndex l ~ IndexOf a
  , CSVMulti l
  )
  => p a -- ^ object to insert into
  -> (PutL l -> z) -- ^ how to index the new SF indeces
  -> Tagged l [PutL l] -- ^ values
  -> WithSF
       ( Map z (IndexOf a)
       , [ ( Maybe (IndexOf a)
           , Text
           , L
               (PutOpOf (CSVProducts l))
               I
               (Maybe (IndexOf a))
               (CSVProducts l)
           )
         ]
       )
bulkInsert proxy proj values' = do
  job <-
    postIngestJ
      [ "operation" .= ("insert" :: Text)
      , "object" .= (toS $ symbolVal proxy :: Text)
      ]
  putJ job values'
  patchJ job ["state" .= ("UploadComplete" :: Text)]
  waitJob ingestP job
  positive <- fmap unTagged $
    withToken $ \tk -> do
      client (Proxy :: Proxy (SuccessfulResultJobApi a l)) tk $ indexOf job
  negative <- fmap unTagged $
    withToken $ \tk -> do
      client (Proxy :: Proxy (FailedResultJobApi a l)) tk $ indexOf job
  pure $ (,)
    do
      fold do
        T (I idx) (L (I (SuccessfulResults t)) z) <- positive
        guard t
        pure $ M.singleton (proj z) idx
    do
      T (I idx) (L (I (FailedResults t)) z) <- negative
      pure (idx, t, z)

data DeleteAny (x :: Symbol)

instance CSVMulti (DeleteAny x) where
  type CSVIndex (DeleteAny x) = IndexOf x
  type CSVProducts (DeleteAny x) = '[]
  csvMulti _ = T (defRT "Id") E

bulkDeleteAny :: forall x. KnownSymbol x => [IndexOf x] -> WithSF ()
bulkDeleteAny xs = bulkDelete (Proxy @x) $ Tagged @(DeleteAny x) $ tie <$> xs

clearObject
  :: forall p x.
  KnownSymbol x
  => p x
  -> Text -- ^ conditions
  -> WithSF ()
clearObject o conditions = do
  xs <- fmap S.fst' $
    S.toList $
      S.map runIdentity $
        S.concat $
          queryStreaming
            o
            do required "Id" (_String . indexP o) :* Nil
            do conditions
  bulkDeleteAny xs
  pure ()

bulkDelete
  :: forall l p a.
  ( KnownSymbol a
  , CSVMulti l
  )
  => p a -- ^ object to delete into
  -> Tagged l [DeleteL l]
  -> WithSF ()
bulkDelete object' ids = do
  job <-
    postIngestJ
      [ "operation" .= ("delete" :: Text)
      , "object" .= (toS $ symbolVal object' :: Text)
      ]
  deleteJ job ids
  patchJ job ["state" .= ("UploadComplete" :: Text)]
  waitJob ingestP job

bulkUpdate
  :: forall l p a.
  ( KnownSymbol a
  , CSVMulti l
  )
  => p a
  -> Tagged l [UpdateL l]
  -> WithSF
       [ ( Maybe (IndexOf a)
           , Text
           , L
               (PutOpOf (CSVProducts l))
               I
               (Maybe (IndexOf a))
               (CSVProducts l)
           )
         ]

bulkUpdate object' values' = do
  job <-
    postIngestJ
      [ "operation" .= ("update" :: Text)
      , "object" .= (toS $ symbolVal object' :: Text)
      ]
  updateJ job values'
  patchJ job ["state" .= ("UploadComplete" :: Text)]
  waitJob ingestP job
  negative <- fmap unTagged $
    withToken $ \tk -> do
      client (Proxy :: Proxy (FailedResultJobApi a l)) tk $ indexOf job
  pure $ do
    T (I idx) (L (I (FailedResults t)) l) <- negative
    pure (idx, t, l)

-- high  level queries

getSingle
  :: forall object value xs.
  ( KnownSymbol object
  , CSVIndexSingle value ~ IndexOf object
  , CSVSingle value
  , IsProductType value xs
  )
  => Text
  -> Stream
       (Of (IndexOf object, value))
       WithSF
       ()
getSingle =
  S.map (getT1 . unTagged . unTagged)
    . bulkQueryS @object @(Single value) 10000

getMulti
  :: forall object value xs.
  ( KnownSymbol object
  , CSVIndex value ~ IndexOf object
  , CSVMulti value
  , CSVProducts value ~ xs
  , IsProductType value xs
  , FromLie xs
  )
  => Text
  -> Stream
       (Of (IndexOf object, value))
       WithSF
       ()
getMulti =
  S.map (tieToProduct . unTagged . unTagged)
    . bulkQueryS @object @value 10000

integralRT :: Integral a => Text -> RT a
integralRT c =
  RT
    do c
    do R $ toField . fromIntegral @_ @Double
    do P $ fmap (floor @Double) . parseField

maybeRT  :: (ToField a, FromField a) => Text -> RT (Maybe a)
maybeRT c =
  RT
    do c
    do R $ maybe (toField @ByteString "#N/A") toField
    do P $ \case
          "#N/A" -> pure Nothing
          x -> Just <$> parseField x

maybeOfRT :: RT a -> RT (Maybe a)
maybeOfRT (RT c (R to') (P parse)) = RT c
      do R $ maybe (toField @ByteString "#N/A") to'
      do P $ \case
          "" -> pure Nothing
          x -> Just <$> parse x
