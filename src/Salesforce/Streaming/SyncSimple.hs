{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Salesforce.Streaming.SyncSimple where

import Control.Lens (over, _3)
import Data.CSV.Product
  ( FromLie,
    IsRecordC,
    L (E, L, T),
    RC (RC),
    RT,
    ToLie,
    defRT,
    lieFromProduct,
    lieToProduct,
    tie,
  )
import Data.Tagged (Tagged (Tagged))
import Generics.SOP
  ( Generic (Code),
    I (..),
    IsProductType,
    NP,
  )
import Generics.SOP.Constraint (Head)
import Generics.SOP.TH (deriveGeneric)
import Protolude hiding (All, Generic)
import Salesforce.API (IndexOf)
import Salesforce.Bulk (bulkDelete, bulkInsert, bulkQueryS, bulkUpdate)
import Salesforce.Login (CallSFT (CallSFT, runSF))
import Servant.CSV.Records (CSVMulti (CSVIndex, CSVProducts, csvMulti))
import Streaming (Of, Stream, hoist)
import Streaming.Prelude qualified as S
import Streaming.Sync.Consume (consumeUpdates)
import Streaming.Sync.Create (Equality, Update, computeUpdatesEq)

data MetricsOutput = MetricsNew Int | MetricsDelete Int | MetricsUpdate Int deriving (Show)

-- data Failure object value = Failure

type Results object value m = Stream (Of MetricsOutput) (Stream (Of (Maybe (IndexOf object), Text, value)) m)

pushMultiChangesSimple ::
  forall object value xs m b.
  ( KnownSymbol object,
    CSVIndex value ~ IndexOf object,
    CSVMulti value,
    CSVProducts value ~ xs,
    IsProductType value xs,
    ToLie xs,
    Monad m,
    MonadIO m,
    FromLie xs
  ) =>
  CallSFT m ->
  Stream (Of (Update (IndexOf object) value)) m b ->
  Results object value m b
pushMultiChangesSimple (CallSFT run) =
  consumeUpdates
    do 10000
    do
      \as -> do
        lift . lift $
          void $
            run $ bulkInsert
              do Proxy @object
              do const ()
              do Tagged @value $ lieFromProduct <$> as
        S.yield $ MetricsNew $ length as
    do
      \as -> do
        lift . lift $
          run $ bulkDelete
            do Proxy @object
            do Tagged @value $ tie <$> as

        S.yield $ MetricsDelete $ length as
    do
      \as -> do
        fs <- lift . lift $ do
          run $ bulkUpdate
            do Proxy @object
            do Tagged @value $ [T (I q) $ lieFromProduct v | ((q, _), v) <- as]
        traverse_
          do \(mi, x, y) -> lift $ S.yield (mi, x, lieToProduct @_ @value y)
          do fs
        S.yield $ MetricsUpdate $ length as
    . do hoist $ lift . lift

updateSalesforceMultiEqC ::
  ( CSVMulti value,
    KnownSymbol object,
    ToLie (CSVProducts value),
    CSVProducts value ~ xs,
    Ord a,
    Eq value,
    CSVIndex value ~ IndexOf object,
    IsProductType value xs,
    MonadIO m,
    FromLie xs
  ) =>
  CallSFT m ->
  Equality value ->
  Stream (Of (a, (IndexOf object, value))) m r ->
  Stream (Of (a, value)) m s ->
  Results object value m (r, s)
updateSalesforceMultiEqC sf eq x y =
  pushMultiChangesSimple sf . S.map snd $ computeUpdatesEq eq x y

data Indexed k v = Indexed k v

newtype UnIndexed v = UnIndexed {unindexed :: v} deriving (Eq, Show)

deriveGeneric ''UnIndexed

type KVRT k v = (IsRecordC v, IsRecordC k, HasRT v, k ~ KeyRT v)

instance KVRT k v => CSVMulti (Indexed k v) where
  type CSVIndex (Indexed k v) = IndexOf (IndexRT v)
  type CSVProducts (Indexed k v) = '[k, v]
  csvMulti _ =
    let (kRT, vRT) = rtsOf (Proxy @v)
     in T (defRT "Id") $ L (RC kRT) $ L (RC vRT) E

type VRT v = (IsRecordC v, HasRT v)

instance VRT v => CSVMulti (UnIndexed v) where
  type CSVIndex (UnIndexed v) = IndexOf (IndexRT v)
  type CSVProducts (UnIndexed v) = '[v]
  csvMulti _ =
    let (_kRT, vRT) = rtsOf (Proxy @v)
     in T (defRT "Id") $ L (RC vRT) E

class HasRT v where
  type IndexRT v :: Symbol
  type KeyRT v :: Type
  rtsOf :: Proxy v -> (NP RT (Head (Code (KeyRT v))), NP RT (Head (Code v)))

updateSalesforce ::
  forall object aux value m k s xs.
  ( KnownSymbol object,
    KVRT aux value,
    Code value ~ '[xs],
    IndexRT value ~ object,
    Ord k,
    Monad m,
    MonadIO m,
    Eq value
  ) =>
  -- | how to run SF query
  CallSFT m ->
  -- | how to compare values
  Equality value ->
  -- | where and order clause
  Text ->
  -- | how to compute k from the SF query
  (aux -> value -> k) ->
  -- | updating signal
  Stream (Of (k, value)) m s ->
  Results object value m s
updateSalesforce run eq s computeK xs =
  hoist
    do S.map (over _3 unindexed)
    do
      snd
        <$> updateSalesforceMultiEqC @(UnIndexed value)
          run
          do eq `on` unindexed
          do
            hoist (runSF run)
              . S.map do
                \(Tagged (Tagged (T (I o) (L (I aux) (L (I v) E))))) ->
                  (computeK aux v, (o, UnIndexed v))
              $ bulkQueryS @object @(Indexed aux value) 10000 s
          do S.map (second UnIndexed) xs
