{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -Wall #-}

module Salesforce.Lib where

import Data.Time
    ( defaultTimeLocale, parseTimeM, UTCTime(UTCTime), ParseTime )
import Protolude hiding (to, toS)
import Protolude.Conv ( toS, StringConv )
import Control.Lens ( prism', Prism' )
import Data.Time.Format.ISO8601 (iso8601Show)



floorSecs :: UTCTime -> UTCTime
floorSecs (UTCTime d s) = UTCTime d $ fromIntegral (floor s :: Int)

parseTime :: (MonadFail m, ParseTime t, StringConv a [Char]) => a -> m t
parseTime = parseTimeM False defaultTimeLocale "%Y-%m-%dT%H:%M:%S%Q+0000" . toS



textTimeP :: Prism' Text UTCTime
textTimeP = prism' (toS . iso8601Show) parseTime 